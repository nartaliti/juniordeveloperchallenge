<?php  

class Product extends dbh {
  protected function getAllProducts() {
    $sql = "SELECT * FROM products";
    $result = $this->connect()->query($sql);
    $numRows = $result->num_rows;
    if($numRows > 0){
        while($row = $result->fetch_assoc()) {
          $data[] = $row;
        }
        return $data;
    }
  }
  protected function createProduct($sku, $name, $price, $type, $size, $weight, $dimensions) {
    switch($type) {
      case "DVD":
        $sql = "INSERT INTO products (SKU, name,price, type, size) VALUES ('$sku', '$name','$price', '$type', '$size')";
        $result = $this->connect()->query($sql);
        header("Location: ../index.php");
      case "BOOK":
        $sql = "INSERT INTO products (SKU, name,price, type, weight) VALUES ('$sku', '$name','$price', '$type', '$weight')";
        $result = $this->connect()->query($sql);
        header("Location: ../index.php");
      case "FURNITURE":
        $sql = "INSERT INTO products (SKU, name,price, type, dimensions) VALUES ('$sku', '$name','$price', '$type', '$dimensions')";
        $result = $this->connect()->query($sql);
        header("Location: ../index.php");
    }
  }
  protected function getProduct($sku) {
    $sql = "SELECT * FROM products WHERE SKU = '$sku'";
    $result = $this->connect()->query($sql);
    return $result;
  }
  protected function deleteProducts($products) {
    foreach($products as $data) {
      $sql = "DELETE FROM products WHERE SKU = '$data'";
      $this->connect()->query($sql);
      header('Location: ../index.php'); 
    }
  }
}