<?php 
  include './includes/dbh.inc.php';
  include './includes/product.inc.php';
  include './classes/viewProduct.class.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./styles/main.css">
  <title>Products</title>
</head>
<body>
  <div class="container header">
    <h1 class="title">Products</h1>
    <form class="mass-delete" action="./classes/mass-delete.class.php" method="POST">
      <input name="products" id="tracker" />
      <button id="delete-product-btn" class="inline float-right button button-danger" type="submit">MASS DELETE</button>
    </form>
    <button class="inline float-right button button-success"><a href='./add-product.php'>ADD</a></button>
  </div>
  <hr>
  <div class='container products'>
    <?php 
      $products = new ViewProduct();
      $products->showAllProducts()
    ?>
  </div>
  <script src="./js/mass-delete.js"></script>
</body>
</html>