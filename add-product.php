<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./styles/main.css">
  <link rel="stylesheet" href="./styles/addProduct.css">
  <title>Add Product</title>
</head>
<body>
  <form method="POST" action="./classes/add-product.class.php" id="product_form">
    <div class="container header">
      <h1 class="title">Add Product</h1>
      <button id="delete-product-btn" class="inline float-right button button-danger" type="button"><a href='./index.php'>Cancel</a></button>
      <button class="inline float-right button button-success" type="submit">Save</button>
    </div>
    <hr>
    <div class="container form">
    <label for="sku">SKU</label>
    <input class="form-control" onchange="skuFormat(event)" name="sku" maxlength="15" id="sku" type="text" placeholder="e.g dsjfs-sfasf-fisf" required/>

    <label for="name">Name</label>
    <input class="form-control" id="name" type="text" name="name" placeholder="Name" required/>

    <label for="price">Price ($)</label>
    <input class="form-control" id="price" type="text" name="price" placeholder="Price" required/>

    <label for="productType">Product Type</label>
    <select id="productType" name="productType" onchange="typeChange(event)" class="form-control" required>
      <option disabled value="" selected>Select Type</option>
      <option value="DVD">DVD</option>
      <option value="BOOK">Book</option>
      <option value="FURNITURE">Furniture</option>
    </select>

    <div id="properties"></div>

    <button class="button" type="submit">Submit</button>
    </div>
  </form>
  <script src="./js/add-product.js"></script>
</body>
</html>