<?php 

include '../includes/dbh.inc.php';
include '../includes/product.inc.php';


class AddProduct extends Product {
  public function addProductHandler() {
    $sku = $_POST['sku'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $type = $_POST['productType'];
    $number = $this->getProduct($sku)->num_rows;
    if($number > 0) {
      echo '<script>alert("There is already a product with that SKU")</script>';
      header('Location: ../add-product.php');
    }
    
    switch($type) {
      case 'DVD':
        $size = $_POST['size'];
        $this->createProduct($sku, $name,"$price$",$type, "$size MB", '', '');
        break;
      case "BOOK":
        $weight = $_POST['weight'];
        $this->createProduct($sku, $name,"$price$",$type, '', "$weight KG", '');
        break;
      case "FURNITURE":
        $height = $_POST['height'];
        $width = $_POST['width'];
        $length = $_POST['length'];
        $this->createProduct($sku, $name,"$price$",$type, '', '', "$height cm X $width cm X $length cm");
        break;
    }
  }
}

$addProduct = new AddProduct();
$addProduct->addProductHandler();