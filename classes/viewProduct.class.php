<?php

class ViewProduct extends Product {
  public function showAllProducts() {
    $products = $this->getAllProducts();
    if(gettype($products) === 'array') {
      $products = array_reverse($products);
    }
    if($products AND count($products) > 0) {
      foreach($products as $data) {
        $sku = $data['SKU'];
        $name = $data['name'];
        $price = $data['price'];
        $size = $data['size'];  
        $dimensions = $data['dimensions'];  
        $weight = $data['weight'];  
        echo '<div class="product">';
        echo "<input class='delete-checkbox' autocomplete='off' onchange='deleteCheckboxHandler(event, \"$sku\")' type='checkbox' />";
        echo "<p class='sku'>$sku</p>";
        echo "<p class='name'>$name</p>";
        echo "<p class='price'>Price: $price</p>";
        echo "<p class='size'>$size</p>";
        echo "<p class='dimensions'>$dimensions</p>";
        echo "<p class='weight'>$weight</p>";
        echo '</div>';  
      } 
    } else {
      echo "<h1 class='error'>No Products Found</h1>";
    }
  }
}