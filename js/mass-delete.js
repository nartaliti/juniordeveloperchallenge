let selectedProducts = [];
const inputTracker = document.querySelector("#tracker");

function inputHandler(values) {
  let inputValue = "";
  for (i = 0; values.length > i; i++) {
    if (inputValue.length > 0) {
      inputValue = inputValue + ` ${values[i]}`;
    } else {
      inputValue = values[i];
    }
    inputTracker.value = inputValue;
  }
}

function deleteCheckboxHandler(event, sku) {
  switch (event.target.checked) {
    case true:
      selectedProducts.push(sku);
      inputHandler(selectedProducts);
      break;
    case false:
      selectedProducts = selectedProducts.filter((productSku) => {
        return productSku !== sku;
      });
      inputHandler(selectedProducts);
      break;
  }
}
