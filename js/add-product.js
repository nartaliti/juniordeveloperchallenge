const skuInput = document.querySelector("#sku");
skuInput.addEventListener("input", function skuChange(event) {
  const value = event.target.value;
  if (value.length > 17) {
    skuInput.value = skuInput.value.substr(0, 17);
  }
});

function skuFormat(event) {
  skuInput.value = event.target.value.match(/.{1,6}/g).join("-");
}

function typeChange(event) {
  const propertiesContainer = document.querySelector("#properties");
  propertiesContainer.innerHTML = " ";
  const title = document.createElement("h3");
  title.innerHTML = "Item Properties";
  title.style.margin = "10px 0";
  propertiesContainer.appendChild(title);

  switch (event.target.value) {
    case "DVD":
      let sizeInput = document.createElement("input");
      sizeInput.setAttribute("id", "size");
      sizeInput.classList.add("form-control");
      sizeInput.setAttribute("name", "size");
      sizeInput.placeholder = "Disc Size (MB)";
      sizeInput.type = "number";
      sizeInput.required = "true";
      return propertiesContainer.appendChild(sizeInput);
    case "BOOK":
      let weightInput = document.createElement("input");
      weightInput.setAttribute("id", "weight");
      weightInput.placeholder = "Book Weight (kg)";
      weightInput.classList.add("form-control");
      weightInput.setAttribute("name", "weight");
      weightInput.type = "number";
      weightInput.required = "true";
      return propertiesContainer.appendChild(weightInput);
    case "FURNITURE":
      let heightInput = document.createElement("input");
      let widthInput = document.createElement("input");
      let lengthInput = document.createElement("input");
      let inputs = [
        { placeholder: "Height (cm)", input: heightInput, id: "height" },
        { placeholder: "Width (cm)", input: widthInput, id: "width" },
        { placeholder: "Length (cm)", input: lengthInput, id: "length" },
      ];
      for (i = 0; i < inputs.length; i++) {
        inputs[i].input.setAttribute("id", inputs[i].id);
        inputs[i].input.placeholder = inputs[i].placeholder;
        inputs[i].input.classList.add("form-control");
        inputs[i].input.setAttribute("name", inputs[i].id);
        inputs[i].input.type = "number";
        inputs[i].input.type.required = "true";
        propertiesContainer.appendChild(inputs[i].input);
      }
      return;
  }
}
